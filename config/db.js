import mysql from "serverless-mysql";

export const pool = mysql({
	config: {
		host: 		process.env.NEXThost,
		user: 		process.env.NEXTuser,
		password: 	process.env.NEXTpassword,
		port: 		3306,
		database: 	process.env.NEXTdatabase
	},
});