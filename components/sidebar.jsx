import Layout from "@/components/layout";
import styles from "@/styles/contact.module.css";
import Link from "next/link";

// Import Material
import { Box, ThemeProvider, createTheme } from '@mui/system';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import RunCircleIcon from '@mui/icons-material/RunCircle';
import PeopleIcon from '@mui/icons-material/People';
import EventIcon from '@mui/icons-material/Event';
import NewspaperIcon from '@mui/icons-material/Newspaper';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import OutputIcon from '@mui/icons-material/Output';
import { Inter } from "next/font/google";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

export default function Sidebar() {
	const t = useTranslations("Sidebar");
	return (
		<>
			<section id="sobre" className="mb-5" >
				<div className="container mt-5">
					<Box className="rounded" sx={{ width: '100%', maxWidth: "100%", bgcolor: '#09377c' }}>
						<nav aria-label="main mailbox folders">
							<List>
								<ListItem disablePadding component="a" href="/admin/usuarios">
									<ListItemButton>
										<ListItemIcon>
											<AccountCircleIcon className="text-white" />
										</ListItemIcon>
										<ListItemText className="text-white" primary={t('usuarios')} />
									</ListItemButton>
								</ListItem>
								<ListItem disablePadding component="a" href="/admin/empleados">
									<ListItemButton>
										<ListItemIcon>
											<PeopleIcon className="text-white" />
										</ListItemIcon>
										<ListItemText className="text-white" primary={t('empleados')} />
									</ListItemButton>
								</ListItem>
								<ListItem disablePadding component="a" href="/admin/actividades">
									<ListItemButton>
										<ListItemIcon>
											<RunCircleIcon className="text-white" />
										</ListItemIcon>
										<ListItemText className="text-white" primary={t('actividades')} />
									</ListItemButton>
								</ListItem>
								<ListItem disablePadding component="a" href="/admin/news">
									<ListItemButton>
										<ListItemIcon>
											<NewspaperIcon className="text-white" />
										</ListItemIcon>
										<ListItemText className="text-white" primary={t('noticias')} />
									</ListItemButton>
								</ListItem>
								<ListItem disablePadding component="a" href="/admin/eventos">
									<ListItemButton>
										<ListItemIcon>
											<EventIcon className="text-white" />
										</ListItemIcon>
										<ListItemText className="text-white" primary={t('eventos')} />
									</ListItemButton>
								</ListItem>
								<ListItem disablePadding component="a" /* onClick={() => signOut()} */>
									<ListItemButton>
										<ListItemIcon>
											<OutputIcon className="text-warning" />
										</ListItemIcon>
										<ListItemText className="text-warning" primary={t('salir')} />
									</ListItemButton>
								</ListItem>
							</List>
						</nav>
					</Box>
				</div>
			</section>
		</>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
