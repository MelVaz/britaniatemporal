import Link from "next/link";
import styles from "@/styles/header.module.css";
import { Inter } from "next/font/google";
import Image from "next/image";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

export default function Adheader() {
	const t = useTranslations("Navbar");
	return (
		<nav className="fixed-top navbar navbar-expand-sm navbar-light bg-light">
			<div className="container d-flex justify-content-between">
				<div className="d-flex align-items-center">
					<a href="/" className="navbar-brand d-flex align-items-center" >
						<img src="/assets/img/logos/logo.png" height="50"></img>
					</a></div>
				<button type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar" className="navbar-toggler">
					<span className="navbar-toggler-icon"></span></button><div id="mynavbar" className="collapse navbar-collapse top-right links">
					<ul className="navbar-nav me-auto"></ul>
					<ul className="navbar-nav me-80">
						<li className="nav-item"><a href="/" className="nav-link" >Inicio</a></li>
						<li><Link className="nav-link" href="/" locale="es">
							<Image
								src="/assets/img/es.png"
								width={15}
								height={10}
								alt="logo windows"
							/> </Link></li>
						<li><Link className="nav-link" href="/" locale="en">
							<Image
								src="/assets/img/en.png"
								width={15}
								height={10}
								alt="logo windows"
							/>
						</Link></li>
					</ul>
				</div>
			</div>
		</nav>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
