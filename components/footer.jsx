import Link from "next/link";
import styles from "@/styles/footer.module.css";
import { Inter } from "next/font/google";
import Image from "next/image";


// Import Translations
import { useTranslations } from 'next-intl';
const inter = Inter({ subsets: ["latin"] });

export default function Footer() {
	const t = useTranslations("Footer");
	const cel = [
		"6141234567"
	]
	return (
		<div className={styles.footer}>
			<footer className={styles.inside}>
				<div className="row">
					<div className="col-md-3">
						<h5>{t('tit1')}</h5>
						<hr className="hr" />
						<ul className="nav flex-column">
							<Link target="_blank" className=" nav-item link-light link-underline-info mb-2" href={`https://www.facebook.com/Britania.Chihuahua/`} locale="en">
								<i className="bi bi-facebook"> {t('facebook')}</i>
							</Link>
							<Link target="_blank" className=" nav-item link-light link-underline-info mb-2" href="https://www.instagram.com/chihuahuabritania/" locale="en">
								<i className="bi bi-instagram"> {t('instagram')}</i>
							</Link>
							<Link target="_blank" className=" nav-item link-light link-underline-info mb-2" href="" locale="en">
								<i className="bi bi-twitter-x"> {t('x')}</i>
							</Link>
							<Link target="_blank" className=" nav-item link-light link-underline-info mb-2" href="https://www.youtube.com/channel/UCVw1Qtj2v1GxxDtqaPC9SjA" locale="en">
								<i className="bi bi-youtube"> {t('youtube')}</i>
							</Link>
						</ul>
					</div>
					<div className="col-md-3">
						<h5> {t('tit2')}</h5>
						<hr className="hr" />
						<ul className="nav flex-column">
							<li className="nav-item mb-2"><i className="bi bi-house"> {t('dir')}</i></li>
							<li className="nav-item mb-2"><i className="bi bi-telephone"> {t('tel')}</i></li>
						</ul>
					</div>
					<div className="col-md-6">
						<h5> {t('tit3')}</h5>
						<hr className="hr" />
						<ul className="nav flex-column">
							{cel.map((tel, index) => (
								<Link target="_blank" key={index} href={`https://api.whatsapp.com/send?phone=${tel}&text=Buenos%20dias`}
									className="nav-item link-light link-underline-info mb-2"><i className="bi bi-whatsapp"> {t('num1')}</i></Link>
							))}
							{cel.map((tel, index) => (
								<Link target="_blank" key={index} href={`https://t.me/+521${tel}`}
									className="nav-item link-light link-underline-info mb-2"><i className="bi bi-telegram"> {t('num2')}</i></Link>
							))}
							<li className="nav-item mb-2"><i className="bi bi-telephone-fill"> {t('num3')}</i></li>
						</ul>
					</div>
				</div>
				<div className="d-flex flex-column flex-sm-row justify-content-center py-4 my-4 border-top">
					<Link href="https://www.marketingtransmedia.com/en" className="nav-link">
						<h3 className={styles.colorDefault}>
							© {new Date().getFullYear()}  Club Britania | {t(`all`)}
						</h3>
					</Link>
				</div>
				<div className="d-flex flex-column flex-sm-row justify-content-center ">
					<Link href="https://www.marketingtransmedia.com/en" className="nav-link">
						<h4 className={styles.colorDefault}>
							{t(`developed`)} 
							<Image className="px-4 logo"
								src="/assets/img/logos/transmedia.png"
								width={300}
								height={120}
								alt="Picture of the author"
							/>
						</h4></Link>
				</div>
			</footer >
		</div >
	);
}