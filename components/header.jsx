import Link from "next/link";
import styles from "@/styles/header.module.css";
import { Inter } from "next/font/google";
import Image from "next/image";

// Import Translations
import { useTranslations } from 'next-intl';
/* db */
import * as React from 'react';
import { loadActividades } from '@/pages/api/actions'
const inter = Inter({ subsets: ["latin"] });

export default function Header() {
	const t = useTranslations("Navbar");
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadActividades().then((p) => setData(p));
		}, []
	)
	return (
		<nav className="fixed-top navbar navbar-expand-sm navbar-light bg-light">
			<div className="container d-flex justify-content-between">
				<div className="d-flex align-items-center">
					<a href="/" className="navbar-brand d-flex align-items-center" >
						<img src="/assets/img/logos/logo.png" height="50"></img>
					</a></div>
				<button type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar" className="navbar-toggler">
					<span className="navbar-toggler-icon"></span></button><div id="mynavbar" className="collapse navbar-collapse top-right links">
					<ul className="navbar-nav me-auto"></ul>
					<ul className="navbar-nav me-80">
						<li className="nav-item"><a href="/" className="nav-link" >Inicio</a></li>
						<div className={styles.dropdown}>
							<li className="nav-item dropdown">
								<a href="/activity" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" className="nav-link dropdown-toggle">
									{t('act')} </a>

								<ul aria-labelledby="navbarDropdown" className="dropdown-menu">
									{data.map((e) => {
										if ((e.Status) == 1) {
											return (
												<li><Link href={`/activity/${e.id}`} className="dropdown-item">{(e.Titulo)}</Link></li>
												
											)
										}
									})}
								</ul>
							</li>
						</div>
						<li className="nav-item"><a href="/news" className="nav-link">{t('news')}</a></li>
						<li className="nav-item"><a href="/events" className="nav-link">{t('events')}</a></li>
						<li><Link className="nav-link" href="/" locale="es">
							<Image
								src="/assets/img/es.png"
								width={15}
								height={10}
								alt="logo windows"
							/> </Link></li>
						<li><Link className="nav-link" href="/" locale="en">
							<Image
								src="/assets/img/en.png"
								width={15}
								height={10}
								alt="logo windows"
							/>
						</Link></li>
						<Link href="/contact" className="get-a-quote"><button type="submit" className="btn btn-dark m-2">{t('contacto')}</button></Link>

					</ul>
				</div>
			</div>
		</nav>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
