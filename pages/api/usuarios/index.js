import { pool } from "@/config/db";

export default async function handler(req, res) {
	/* console.log(req); */
	if (req.method === 'GET') {
		try {
			const results = await pool.query("SELECT id as id , email as Email, password as Contraseña, status as Status FROM users WHERE status != 0 ");
/* 			console.log(results);
 */			return res.status(200).json(results);
		} catch (error) {
			return res.status(500).json(
				{ message: error.message }
			);
		}
	} else if (req.method === 'POST'){
		try {
			const { email, password } = await req.body;
			const result = await pool.query("INSERT INTO users SET ?", {
				email,
				password
			});
			return res.status(200).json({ email, password, id: result.insertId });
		} catch (error) {
			return 	res.status(500).json(			
				{ message: error.message }
			);
		}
	} else {}
}
