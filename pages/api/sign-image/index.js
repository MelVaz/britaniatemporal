import { v2 as cloudinary } from "cloudinary";

cloudinary.config({
	cloud_name: process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME,
	api_key: process.env.NEXT_PUBLIC_CLOUDINARY_API_KEY,
	api_secret: process.env.NEXT_PUBLIC_CLOUDINARY_API_SECRET,
});

export default async function handler(req, res) {
	const { paramsToSign } = req.body;
	/* console.log("body"+req.body);  */
	const signature = cloudinary.utils.api_sign_request(paramsToSign, process.env.NEXT_PUBLIC_CLOUDINARY_API_SECRET);

	res.status(200).json({ signature });
	/* console.log(signature);  */
}
	/* export default async function handler2(req, res) {
	result = await cloudinary.api.resources()
.resources(
		{ type: 'upload', 
		prefix: 'shirt' })
		.then(result=>console.log(result)); }*/


