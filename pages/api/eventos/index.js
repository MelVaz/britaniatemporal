import { pool } from "@/config/db";

export default async function handler(req, res) {
	/* console.log(req); */
	if (req.method === 'GET') {
		try {
			const results = await pool.query("SELECT id as id , title as Titulo, description as Descripcion,fecha as Fecha, status as Status FROM eventos WHERE status != 0 ");
/* 			console.log(results);
 */			return res.status(200).json(results);
		} catch (error) {
			return res.status(500).json(
				{ message: error.message }
			);
		}
	} else if (req.method === 'POST'){
		try {
			const { title, description, fecha } = await req.body;
			const result = await pool.query("INSERT INTO eventos SET ?", {
				title,
				description,
				fecha
			});
			return res.status(200).json({ title, description, fecha, id: result.insertId });
		} catch (error) {
			return 	res.status(500).json(			
				{ message: error.message }
			);
		}
	} else {}
}
