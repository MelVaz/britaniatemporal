import { pool } from "@/config/db";

export default async function handler(req, res) {
	console.log(req);
	if (req.method === 'GET') {
		try {
			const results = await pool.query("SELECT id as id , nombre as Nombre, puesto as Puesto,tipo as Tipo, status as Status FROM empleados WHERE status != 0 ");
/* 			console.log(results);
 */			return res.status(200).json(results);
		} catch (error) {
			return res.status(500).json(
				{ message: error.message }
			);
		}
	} else if (req.method === 'POST'){
		try {
			const { nombre, puesto, tipo } = await req.body;
			const result = await pool.query("INSERT INTO empleados SET ?", {
				nombre,
				puesto,
				tipo
			});
			return res.status(200).json({ nombre, puesto, tipo, id: result.insertId });
		} catch (error) {
			return 	res.status(500).json(			
				{ message: error.message }
			);
		}
	} else {}
}
