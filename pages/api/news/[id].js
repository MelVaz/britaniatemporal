import { NextResponse } from "next/server";
import { pool } from "@/config/db";

export default async function handler(req, res) {
	const { id } = req.query
	if (req.method === 'GET') {
		try {
			const result = await pool.query("SELECT * FROM news WHERE id = ? ", [req.query?.id,]);
			return res.status(200).json(result[0]);
		} catch (error) {
			return res.status(500).json({ message: error.message });
		}
	} else if (req.method === 'DELETE') {
		try {
			await pool.query("UPDATE news SET status = 0 WHERE id = ?", [req.query?.id]);
			return res.status(200).json({}, { status: 204 });
		} catch (error) {
			return res.status(500).json({ message: error.message });
		}
	} else if (req.method === 'POST') {
		const { title, description, fecha } = await req.body;
		try {
			await pool.query(`UPDATE news SET title = '${title}', description = '${description}',fecha = '${fecha}' WHERE id = ?`, [req.query?.id]);
			return res.status(200).json({}, { status: 204 });
		} catch (error) {
			return res.status(500).json({ message: error.message });
		}
	} else if (req.method === 'PUT') {
		try {
			const { status} = await req.body;
			await pool.query(`UPDATE news SET status = '${status}' WHERE id = ?`, [req.query?.id]);
			return res.status(200).json({}, { status: 204 });
		} catch (error) {
			return res.status(500).json({ message: error.message });
		}
	}
}
