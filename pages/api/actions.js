import axios from "axios";
/* Usuarios */
export async function loadUsuarios() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/usuarios");
	return data;
}
export async function loadUsuario(id) {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/usuarios/" + id);
	return data;
}
/* News */
export async function loadNews() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/news");
	/* console.log(data);  */
	return data;
}
export async function loadNew(id) {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/news/" + id);
	return data;
}

/* Events */
export async function loadEvents() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/eventos");
	return data;
}
export async function loadEvent(id) {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/eventos/" + id);
	return data;
}

/* Empleados */
export async function loadEmpleados() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/empleados");
	return data;
}
export async function loadEmpleado(id) {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/empleados/" + id);
	return data;
}

/* Actividades */
export async function loadActividades() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/actividades");
	return data;
}
export async function loadActividad(id) {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/actividades/" + id);
	return data;
}

/* img */
export async function loadImgs() {
	const { data } = await axios.get("https://britaniatemporal.vercel.app/api/temporal");
	return data;
}
