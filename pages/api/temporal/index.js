import { pool } from "@/config/db";
import { v2 as cloudinary } from "cloudinary";

cloudinary.config({
	cloud_name: process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME,
	api_key: process.env.NEXT_PUBLIC_CLOUDINARY_API_KEY,
	api_secret: process.env.NEXT_PUBLIC_CLOUDINARY_API_SECRET,
});

export default async function handler(req, res) {
	/* console.log(req); */
	if (req.method === 'GET') {
		try {
			const results = await cloudinary.api.resources({context:true,max_results:100})
		return res.status(200).json(results);
		} catch (error) {
			return res.status(500).json(
				{ message: error.message }
			);
		}
	}
}
