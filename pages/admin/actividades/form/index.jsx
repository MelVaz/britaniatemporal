import Sidebar from "@/components/sidebar";
import * as React from 'react';
import { redirect } from 'next/navigation';
import Adlayout from "@/components/adlayout";
import styles from "@/styles/form.module.css";
// Import Translations
import { useTranslations } from 'next-intl';
import { Inter } from "next/font/google";
const inter = Inter({ subsets: ["latin"] });
/* db */
import { loadActividad } from '@/pages/api/actions';
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useSearchParams } from 'next/navigation'
/* Imagess */
import { CldImage } from 'next-cloudinary';
import { CldUploadWidget } from 'next-cloudinary';
import { useState } from "react";


export default function Form() {
	const [resource, setResource] = useState();

	const t = useTranslations("Adnews");
	const searchParams = useSearchParams()
	const search = searchParams.get('id')
	const router = useRouter();
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadActividad(search).then((p) => setData(p));
		}, []
	)
	const edit = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/actividades/" + search, data);
			router.push('/admin/actividades')
		} catch (error) {
			console.error(error);
		}
	};
	const nuevo = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/actividades/", data);
			router.push('/admin/actividades')
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<>
			<Adlayout >
				<main id="main">
					<section id="form" >
						<div className="row mt-5">
							<div className="col">
								<h1 className="h2 m-4">{t('dashboard')}</h1>
								<Sidebar />
							</div>
							<div className="col-9">
								<div className={styles.form}>
									<div className="alert alert-info" role="alert"> {t('actividades')}</div>
									<form className="row g-3 p-5" >
										<div className="col-12">
											<label for="inputAddress" className="form-label">Titulo</label>
											<input onChange={e => setData({ ...data, 'title': e.target.value })}
												type="text" value={data.title} className="form-control" id="title" />
										</div>
										<div className="col-md-12">
											<label for="inputPassword4" className="form-label">Descripción</label>
											<textarea onChange={e => setData({ ...data, 'description': e.target.value })}
												value={data.description}
												className="form-control" id="description" rows="3"></textarea>
										</div>
										<CldUploadWidget uploadPreset="news_portada" signatureEndpoint="/api/sign-image"
											options={{
												sources: ['local', 'url', 'unsplash'],
												multiple: false,
												maxFiles: 1
											}}>
											{({ open }) => {
												return (
													<button className="btn btn-danger" onClick={(e) => {
														e.preventDefault();
														open();
													}}>
														Horario
													</button>
												);
											}
											}
										</CldUploadWidget>
										<div className="row m-3">
											<div className="col-md">
												<span>Imagen actual:  </span>
												<CldImage
													width="150"
													height="150"
													src="https://res.cloudinary.com/dybaqvxrs/image/upload/v1717526653/news/portada/6_xkatl3.jpg"
													alt="Description of my image"
												/>
											</div>
											<div className="col-md ">
												<span>Imagen nueva: </span>
												<CldImage
													width="150"
													height="150"
													src="https://res.cloudinary.com/dybaqvxrs/image/upload/v1717526653/news/portada/6_xkatl3.jpg"
													alt="Description of my image"
												/>
											</div>
										</div>
										<div className="col-12">
											<button onClick={(e) => { search != null ? edit(e) : nuevo(e) }} className="btn btn-primary">Guardar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</section>
				</main>
			</Adlayout >
		</>
	)
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../../../../messages/${context.locale}.json`)).default
		}
	};
}
