import Link from "next/link";
import Adlayout from "@/components/adlayout";
import Sidebar from "@/components/sidebar";
import { useTranslations } from 'next-intl';
import * as React from 'react';
import { DataGrid, GridActionsCellItem } from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@material-ui/icons/Edit';
import DoDisturbOnIcon from '@mui/icons-material/DoDisturbOn';
import PlayCircleFilledWhiteIcon from '@mui/icons-material/PlayCircleFilledWhite';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { pink } from '@mui/material/colors';
import { blue } from '@mui/material/colors';
import { redirect } from 'next/navigation';
/* db */
import { loadUsuarios } from '@/pages/api/actions';
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";

export default function Usuarios() {
	const router = useRouter();
	const t = useTranslations("Adnews");
	const [rows, setRows] = React.useState([]);
	React.useEffect(
		() => {
			loadUsuarios().then((p) => setRows(p));
		}, []
	)
	const deleteUsuarios = async (id) => {
		try {
			await axios.delete("/api/usuarios/" + id);
			setRows((prevRows) => prevRows.filter((row) => row.id !== id));
			toast.success("Eliminado");
		} catch (error) {
			console.error(error);
		}
	};
	const toggleActive = async (id) => {
		try {
			await axios.put("/api/usuarios/" + id, { status: 1 });
			setRows((prevRows) =>
				prevRows.map((row) =>
					row.id === id ? { ...row, Status: 1 } : row,
				),
			);
			toast.success("Cambio de status");
			/* 	router.refresh(); */
		} catch (error) {
			console.error(error);
		}
	};
	const toggleDeactivate = async (id) => {
		try {
			await axios.put("/api/usuarios/" + id, { status: 2 });
			setRows((prevRows) =>
				prevRows.map((row) =>
					row.id === id ? { ...row, Status: 2 } : row,
				),
			);
			toast.success("Cambio de status");
			/* router.refresh(); */
		} catch (error) {
			console.error(error);
		}
	};
	const toForm = React.useCallback(
		(id) => () => {
			<Link href="/usuarios/form" />
		},
		[],
	);
	const columns = React.useMemo(
		() => [
			{ field: 'id', type: 'number' },
			{ field: 'Email', type: 'string', label: 'Email', width: 180 },
			{
				field: 'Status', type: 'int', width: 120,
				valueGetter: (status) => {
					if (status == 1) {
						return "Activo";

					} else if (status == 2) {
						return "Suspendido";
					}
				},
				renderCell: (params) => params.value != "Suspendido" ? <span classnAME="badge bg-success">{params.value}</span> : <span classnAME="badge bg-danger">{params.value}</span>,
			},

			{
				field: 'actions',
				type: 'actions',
				width: "120",
				getActions: (params) => [
					<GridActionsCellItem key={params.id}
						icon={<DeleteIcon sx={{ color: pink[900] }} />}
						label="Delete"
						onClick={() => deleteUsuarios(params.id)}
					/>,
					<Link key={params.id} href={{
						pathname: '/admin/usuarios/form/',
						query: { id: params.id },
					}}>
						<GridActionsCellItem
							icon={<EditIcon sx={{ color: blue[900] }} />}
							label="Edit" />
					</Link>,
					<GridActionsCellItem key={params.id}
						icon={<PlayCircleFilledWhiteIcon />}
						label={t('togAc')}
						onClick={() => toggleActive(params.id)}
						showInMenu
					/>,
					<GridActionsCellItem key={params.id}
						icon={<DoDisturbOnIcon />}
						label={t('togDe')}
						onClick={() => toggleDeactivate(params.id)}
						showInMenu
					/>
				],
			},
		],
		[deleteUsuarios, toggleActive, toggleDeactivate],
	);
	return (
		<>
			<Adlayout >
				<main id="main">
					<section id="admin" className="contact">
						<h1 className="d-flex justify-content-center mt-5">{t('dashboard')}</h1>
						<div className="row">
							<div className="col">
								<Sidebar />
							</div>
							<div className="col-9">
								<div className="row">
									<div className="col">
										<div className="alert alert-info" role="alert">{t('tabla')} / {t('usuarios')}</div>
									</div>
									<div className="col">
										<Link href="/admin/usuarios/form/">
											<button type="button" className="btn mt-2 btn-success"><AddCircleIcon /></button>
										</Link>
									</div>
								</div>
								<div style={{ height: 'auto', width: 'auto' }}>
									<DataGrid autoHeight={true} columns={columns} rows={rows} rowsPerPageOptions={[5, 10, 20]} />
								</div>
							</div>
						</div>
					</section>
				</main>
			</Adlayout>
		</>
	)
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../../../messages/${context.locale}.json`)).default
		}
	};
}
