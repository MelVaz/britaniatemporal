import Sidebar from "@/components/sidebar";
import * as React from 'react';
import { redirect } from 'next/navigation';
import Adlayout from "@/components/adlayout";
import styles from "@/styles/form.module.css";
// Import Translations
import { useTranslations } from 'next-intl';
import { Inter } from "next/font/google";
const inter = Inter({ subsets: ["latin"] });
/* db */
import { loadUsuario } from '@/pages/api/actions';
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useSearchParams } from 'next/navigation'


export default function Form() {
	const t = useTranslations("Adnews");
	const searchParams = useSearchParams()
	const search = searchParams.get('id')
	const router = useRouter();
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadUsuario(search).then((p) => setData(p));
		}, []
	)
	const edit = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/usuarios/" + search, data);
			router.push('/admin/usuarios')
		} catch (error) {
			console.error(error);
		}
	};
	const nuevo = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/usuarios/", data);
			router.push('/admin/usuarios')
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<>
			<Adlayout >
				<main id="main">
					<section id="form" >
						<div className="row mt-5">
							<div className="col">
								<h1 className="h2 m-4">{t('dashboard')}</h1>
								<Sidebar />
							</div>
							<div className="col-9">
								<div className={styles.form}>
									<div className="alert alert-info" role="alert"> {t('usuarios')}</div>
									<form className="row g-3 p-5" >
										<div className="col-12">
											<label for="inputAddress" className="form-label">Titulo</label>
											<input onChange={e => setData({ ...data, 'email': e.target.value })}
												type="email" value={data.email} className="form-control" id="email" />
										</div>
										<div className="col-12">
											<label for="inputAddress" className="form-label">Titulo</label>
											<input onChange={e => setData({ ...data, 'password': e.target.value })}
												type="password" value={data.password} className="form-control" id="password" />
										</div>
										<div className="col-12">
											<button onClick={(e) => { search != null ? edit(e) : nuevo(e) }} className="btn btn-primary">Guardar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</section>
				</main>
			</Adlayout >
		</>
	)
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../../../../messages/${context.locale}.json`)).default
		}
	};
}
