import Sidebar from "@/components/sidebar";
import * as React from 'react';
import { redirect } from 'next/navigation';
import Adlayout from "@/components/adlayout";
import styles from "@/styles/form.module.css";
// Import Translations
import { useTranslations } from 'next-intl';
import { Inter } from "next/font/google";
const inter = Inter({ subsets: ["latin"] });
/* db */
import { loadNew } from '@/pages/api/actions';
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useSearchParams } from 'next/navigation'
/* Imagess */
import { CldImage } from 'next-cloudinary';
import { CldUploadWidget } from 'next-cloudinary';


export default function Form() {
	const t = useTranslations("Adnews");
	const searchParams = useSearchParams()
	const search = searchParams.get('id')
	const router = useRouter();
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadNew(search).then((p) => setData(p));
		}, []
	)
	const edit = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/news/" + search, data);
			router.push('/admin/news')
		} catch (error) {
			console.error(error);
		}
	};
	const nuevo = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/news/", data);
			router.push('/admin/news')
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<>
			<Adlayout >
				<main id="main">
					<section id="form" >
						<div className="row mt-5">
							<div className="col">
								<h1 className="h2 m-4">{t('dashboard')}</h1>
								<Sidebar />
							</div>
							<div className="col-9">
								<div className={styles.form}>
									<div className="alert alert-info" role="alert"> {t('news')}</div>
									<form className="row g-3 p-5" >
										<div className="col-12">
											<label for="inputAddress" className="form-label">Titulo</label>
											<input onChange={e => setData({ ...data, 'title': e.target.value })}
												type="text" value={data.title} className="form-control" id="title" />
										</div>
										<div className="col-md-12">
											<label for="inputPassword4" className="form-label">Descripción</label>
											<textarea onChange={e => setData({ ...data, 'description': e.target.value })}
												value={data.description}
												className="form-control" id="description" rows="3"></textarea>
										</div>
										<div className="col-md-12">
											<label for="inputPassword4" className="form-label">Fecha</label>
											<input onChange={e => setData({ ...data, 'fecha': e.target.value })}
												type="date" value={data.fecha} className="form-control" id="fecha" />
										</div>
										<div className="col-12">
											<button onClick={(e) => { search != null ? edit(e) : nuevo(e) }} className="btn btn-primary">Guardar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</section>
				</main>
			</Adlayout >
		</>
	)
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../../../../messages/${context.locale}.json`)).default
		}
	};
}
