import Sidebar from "@/components/sidebar";
import * as React from 'react';
import { redirect } from 'next/navigation';
import Adlayout from "@/components/adlayout";
import styles from "@/styles/form.module.css";
// Import Translations
import { useTranslations } from 'next-intl';
import { Inter } from "next/font/google";
const inter = Inter({ subsets: ["latin"] });
/* db */
import { loadEmpleado } from '@/pages/api/actions';
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useSearchParams } from 'next/navigation'
/* Imagess */
import { CldImage } from 'next-cloudinary';
import { CldUploadWidget } from 'next-cloudinary';
import { useState } from "react";


export default function Form() {
	const [resource, setResource] = useState();

	const t = useTranslations("Adnews");
	const searchParams = useSearchParams()
	const search = searchParams.get('id')
	const router = useRouter();
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadEmpleado(search).then((p) => setData(p));
		}, []
	)
	const edit = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/empleados/" + search, data);
			router.push('/admin/empleados')
		} catch (error) {
			console.error(error);
		}
	};
	const nuevo = async (e) => {
		e.preventDefault();
		try {
			await axios.post("/api/empleados/", data);
			router.push('/admin/empleados')
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<>
			<Adlayout >
				<main id="main">
					<section id="form" >
						<div className="row mt-5">
							<div className="col">
								<h1 className="h2 m-4">{t('dashboard')}</h1>
								<Sidebar />
							</div>
							<div className="col-9">
								<div className={styles.form}>
									<div className="alert alert-info" role="alert"> {t('empleados')}</div>
									<form className="row g-3 p-5" >
										<div className="col-12">
											<label for="inputAddress" className="form-label">Nombre Completo</label>
											<input onChange={e => setData({ ...data, 'nombre': e.target.value })}
												type="text" value={data.nombre} className="form-control" id="nombre" />
										</div>
										<div className="col-md-12">
											<label for="inputPassword4" className="form-label">Puesto</label>
											<textarea onChange={e => setData({ ...data, 'puesto': e.target.value })}
												value={data.puesto}
												className="form-control" id="puesto" rows="3"></textarea>
										</div>
										<div className="col-md-12">
											<label for="inputPassword4" className="form-label">Tipo</label>
											<select className="form-select" id="tipo" value={data.tipo} onChange={e => setData({ ...data, 'tipo': e.target.value })}>
												<option selected>Elija a donde pertece el empleado</option>
												<option value="Presidente">Presidente</option>
												<option value="Vocales">Vocales</option>
												<option value="Actividad">Actividad</option>
											</select>
										</div>
										<CldUploadWidget uploadPreset="news_portada" signatureEndpoint="/api/sign-image"
											options={{
												sources: ['local', 'url', 'unsplash'],
												multiple: false,
												maxFiles: 1
											}}>
											{({ open }) => {
												return (
													<button className="btn btn-danger" onClick={(e) => {
														e.preventDefault();
														open();
													}}>
														Portada
													</button>
												);
											}
											}
										</CldUploadWidget>
										<div className="row m-3">
											<div className="col-md">
												<span>Imagen actual:  </span>
												<CldImage
													width="150"
													height="150"
													src="https://res.cloudinary.com/dybaqvxrs/image/upload/v1717526653/news/portada/6_xkatl3.jpg"
													alt="Description of my image"
												/>
											</div>
											<div className="col-md ">
												<span>Imagen nueva: </span>
												<CldImage
													width="150"
													height="150"
													src="https://res.cloudinary.com/dybaqvxrs/image/upload/v1717526653/news/portada/6_xkatl3.jpg"
													alt="Description of my image"
												/>
											</div>
										</div>
										<div className="col-12">
											<button onClick={(e) => { search != null ? edit(e) : nuevo(e) }} className="btn btn-primary">Guardar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</section>
				</main>
			</Adlayout >
		</>
	)
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../../../../messages/${context.locale}.json`)).default
		}
	};
}
