import Layout from "@/components/layout";
import { Inter } from "next/font/google";
import styles from "@/styles/activity.module.css";
import Link from "next/link";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import 'swiper/css/navigation';

// import required modules
import { EffectCoverflow, Pagination, Navigation } from 'swiper/modules';
/* db */
import * as React from 'react';
import { loadActividad } from '@/pages/api/actions'
import { loadActividades } from '@/pages/api/actions'

export default function Activity({ data }) {
	const t = useTranslations("Activity");
	const [data2, setData2] = React.useState([]);
	React.useEffect(
		() => {
			loadActividades().then((p) => setData2(p));
		}, []
	)
	return (
		<>
			<Layout>
				<section id="sobre" >
					<div className="container ">
						<div className={styles.titulo}>
							<h1 className="m-5 d-flex justify-content-center text-uppercase fst-italic fw-bold"><span>{data.title}</span></h1>
						</div>
						<div className={styles.half1}>
							<div className="row " >
								<div className="col-md-6">
									<div className="card-transparent ">
										<div className="card-body">
											<h3 className="d-flex justify-content-center m-3">
												{data.title}
											</h3>
											<div>
												{data.description.split('.').map(d => <h5 className="subtitle m-2">{d}.</h5>)}
											</div>
										</div>
									</div>
								</div>
								<div className="col">
									<img src="/assets/img/index/1.webp" className="img-fluid mx-auto d-block" />
								</div>
							</div>
						</div>
					</div>
				</section>
				<section id="horarios" className={styles}>
					<div className="container ">
						<h3 className="mt-5 d-flex justify-content-center">Horarios</h3><br />
						<hr className="hr" />
						<img src="/assets/img/index/1.webp" className="img-fluid rounded mx-auto d-block" />

					</div>
				</section>
				<section id="profesores" className={styles}>
					<div className="container ">
						<h3 className="mt-5 d-flex justify-content-center">Profesores</h3><br />
						<hr className="hr" />
						<div className="row" >
							<div className="col-md-5">
								<img src="/assets/img/index/1.webp" className="img-thumbnail" />
							</div>
							<div className="col-md-6">
								<div className="card-transparent ">
									<div className="card-body p-5">
										<h3 className="mb-3">Jane Doe</h3>
										<h5 className="subtitle mb-3">
											Lorem ipsum dolor sit amet consectetur. Donec nulla orci quis hendrerit mattis tellus quis sit nulla. Morbi fringilla libero nunc nunc sodales massa ultrices scelerisque eget. Turpis adipiscing in auctor elit et. Elit tristique suscipit nascetur eget quam elementum mattis.Lorem ipsum dolor sit amet consectetur. Donec nulla orci quis hendrerit mattis tellus quis sit nulla. Morbi fringilla libero nunc nunc sodales massa ultrices scelerisque eget. Turpis adipiscing in auctor elit et. Elit tristique suscipit nascetur eget quam elementum mattis.
										</h5>
									</div>
								</div>
							</div>
						</div>
						<h3 className="mt-5 d-flex justify-content-center">Staff</h3><br />
						<Swiper autoplay={{
							delay: 3500,
							disableOnInteraction: false,
						}}
							modules={[Pagination, Navigation]}
							pagination={{
								clickable: true,
							}}
							navigation={true}
							spaceBetween={30}
							slidesPerView={1}
							breakpoints={{
								320: {
									slidesPerView: 1,
									spaceBetween: 15,
								},
								640: {
									slidesPerView: 2,
									spaceBetween: 20,
								},
								768: {
									slidesPerView: 4,
									spaceBetween: 40,
								},
								1800: {
									slidesPerView: 4,
									spaceBetween: 50,
								},
							}}>
							<SwiperSlide>
								<div className='text-center justify-content-center'>
									<div className="card" >
										<img className={styles.act} src="/assets/img/index/1.webp" />
										<Link href="/activity" className={styles.underline}><span> {t('title')}</span></Link>
									</div>
								</div>
							</SwiperSlide>
							<SwiperSlide>
								<div className='text-center justify-content-center'>
									<div className="card" >
										<img className={styles.act} src="/assets/img/index/1.webp" />
										<Link href="/activity" className={styles.underline}><span> {t('title')}</span></Link>
									</div>
								</div>
							</SwiperSlide>
							<SwiperSlide>
								<div className='text-center justify-content-center'>
									<div className="card" >
										<img className={styles.act} src="/assets/img/index/1.webp" />
										<Link href="/activity" className={styles.underline}><span> {t('title')}</span></Link>
									</div>
								</div>
							</SwiperSlide>
							<SwiperSlide>
								<div className='text-center justify-content-center'>
									<div className="card" >
										<img className={styles.act} src="/assets/img/index/1.webp" />
										<Link href="/activity" className={styles.underline}><span> {t('title')}</span></Link>
									</div>
								</div>
							</SwiperSlide>
							<br />
						</Swiper>
					</div>
				</section>
				<section id="profesores" >
					<div className="container mb-5">
						<h3 className="mt-5 d-flex justify-content-center">Galeria</h3><br />
						<hr className="hr" />
						<Swiper
							effect={'coverflow'}
							grabCursor={true}
							centeredSlides={true}
							slidesPerView={3}
							initialSlide={1}
							coverflowEffect={{
								rotate: 50,
								stretch: 0,
								depth: 100,
								modifier: 1,
								slideShadows: true,
							}}
							pagination={true}
							modules={[EffectCoverflow, Pagination]}
							className={styles.swiper2}
						>
							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>

							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>

							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>
						</Swiper>
					</div>
				</section>
				<section id="profesores" >
					<div className="container">
						<h3 className="mt-5 d-flex justify-content-center  pt-5">Actividades deportivas</h3><br />
						<h4 className="d-flex justify-content-center text-secondary"><span>{t('club')}</span></h4>
						<hr className="hr" />
						<div className="row  m-5">
							{data2.map((e) => {
								if ((e.Status) == 1) {
									return (
										<div className="col ">
											<Link href={`/activity/${e.id}`}><button type="button" className="btn btn-outline-primary">{(e.Titulo)}</button></Link>
										</div>
									)
								}
							})}
						</div>
					</div>
				</section>
			</Layout>
		</>
	);
}
export async function getServerSideProps(props) {
	const id = await loadActividad(props.params.id);
/* 	console.log(id);
 */	return { props: { data: id, messages: (await import(`../../messages/${props.locale}.json`)).default } };
}


