import Image from "next/image";
import Link from "next/link";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import Layout from "@/components/layout";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Pagination, Navigation } from 'swiper/modules';
/* db */
import * as React from 'react';
import { loadEmpleados } from '@/pages/api/actions'
import { loadActividades } from '@/pages/api/actions'

export default function Home() {
	const t = useTranslations("Home");
	/*Empleados */
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadEmpleados().then((p) => setData(p));
		}, []
	)
	/*Actividades */
	const [data2, setData2] = React.useState([]);
	React.useEffect(
		() => {
			loadActividades().then((p) => setData2(p));
		}, []
	)
	return (
		<>
			<Layout>
				<section id="banner" className="banner">
					<Swiper
						slidesPerView={1}
						spaceBetween={30}
						breakpoints={{
							320: {
								slidesPerView: 1,
								spaceBetween: 0,
							},
							640: {
								slidesPerView: 1,
								spaceBetween: 0,
							},
							768: {
								slidesPerView: 1,
								spaceBetween: 0,
							},
							1800: {
								slidesPerView: 1,
								spaceBetween: 50,
							},
						}}
						loop={true}
						pagination={{
							clickable: true,
						}}
						navigation={true}
						modules={[Pagination, Navigation]}
						className="mySwiper">
						<SwiperSlide>
							<img src="/assets/img/index/banner1.png" width={100}
								height={100} className={styles.banner} />
							<p className={styles.bannerp}>
								<img src="/assets/img/logos/logo.png" width={200}
									height={300} className={styles.banner} />
							</p>
						</SwiperSlide>
						<SwiperSlide>
							<img src="/assets/img/index/banner2.png" width={200}
								height={300} className={styles.banner} />
							<p className={styles.bannerp}>
								<img src="/assets/img/logos/logo.png" width={200}
									height={300} className={styles.banner} />
							</p>
						</SwiperSlide>
					</Swiper>
				</section>
				<main role="main" className="container">
					<section id="sobre" className={styles.half}>
						<div className="container ">
							<div className="row">
								<div className="col-sm-6">
									<img src="/assets/img/index/acercade.jpg" className="img-thumbnail" />
								</div>
								<div className="col-sm-6">
									<div className="card-transparent ">
										<div className="card-body ">
											<h3 className="mb-3">Tenemos todo para que lleves a cabo tu actividad física o social, en las mejores condiciones. </h3>
											<h5 className="subtitle mb-3">Los servicios de nuestro club social y deportivo incluyen las diversas áreas para la practica de deportes como tenis, raquetbol, fútbol de sala, natación, karate, fitnees, spining, baile moderno, cardio jump, y aerobicos, entre otros. Asimismo tenemos áreas sociales para la convivencia de los socios y miembros, ademàs del consumo de alimentos y bebidas como el Restaurante Cocos, el Snack Bar y un tercer snack ubicado a un costado de las canchas de raquetbol.</h5>
										</div>
									</div>
								</div>
								<div className={styles.divhalf}>
									<div className="d-flex flex-row-reverse">
										<div className="p-2 fw-bolder"><span className={styles.underline}>9 Canchas de Tennis</span></div>
										<div className="p-2 fw-bolder"><span className={styles.underline}>20 torneos al año</span></div>
										<div className="p-2 fw-bolder"><span className={styles.underline}>+1000 miembros</span></div>
										<div className="p-2 fw-bolder"><span className={styles.underline}>+32 años de experiencia</span></div>
									</div>
								</div>
							</div>

						</div>
					</section>
					<section id="actividades" className="actividades">
						<h3 className="mt-5 d-flex justify-content-center">{t('act')}</h3><br />
						<h4 className="d-flex justify-content-center text-secondary"><span>{t('club')}</span></h4>
						<hr className="hr" />
						<Swiper autoplay={{
							delay: 3500,
							disableOnInteraction: false,
						}}
							modules={[Pagination, Navigation]}
							pagination={{
								clickable: true,
							}}
							navigation={true}
							spaceBetween={30}
							slidesPerView={1}
							breakpoints={{
								320: {
									slidesPerView: 1,
									spaceBetween: 15,
								},
								640: {
									slidesPerView: 2,
									spaceBetween: 20,
								},
								768: {
									slidesPerView: 4,
									spaceBetween: 40,
								},
								1800: {
									slidesPerView: 4,
									spaceBetween: 50,
								},
							}}>

							{data2.map((e) => {
								if ((e.Status) == 1) {
									return (
										<SwiperSlide>
											<div className='text-center justify-content-center'>
												<div className="card" >
													<img className={styles.act} src="/assets/img/index/1.webp" />
													<div className={styles.azul}>
														<Link href={`/activity/${e.Titulo}`} className={styles.underline}><span>{(e.Titulo)}</span></Link>
													</div>
												</div>
											</div>
										</SwiperSlide>
									)
								}
							})}

							{/* 			<SwiperSlide>
								<div className='text-center justify-content-center'>

									<div className="card" >
										<img className={styles.act} src="/assets/img/index/1.webp" />
										<div className={styles.azulm}>
											<Link href="/about" className={styles.underline}><span> {t('padel')}</span></Link>
										</div>
									</div>

								</div>
							</SwiperSlide> */}
							<br />
						</Swiper>
					</section>
					<section id="consejo1" className="consejo1">
						<h3 className="mt-5 d-flex justify-content-center">Consejo directivo</h3><br />
						<hr className="hr" />
						<h4 className="m-3 d-flex justify-content-center text-secondary"><span>Presidente</span></h4>

						{data.map((e) => {
							if ((e.Status && e.Tipo == "Presidente") == 1) {
								return (
									<div className=" d-flex justify-content-center">
										<div className=" ">
											<svg className="bd-placeholder-img rounded-circle" width="250" height="250" xmlns="/assets/img/index/1.webp" role="img" aria-label="Placeholder: 140x140" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"></rect><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text></svg>
											<h2 className=" d-flex justify-content-center">{(e.Nombre)}</h2>
											<p className=" d-flex justify-content-center"> {(e.Puesto)}</p>
										</div>
									</div>
								)
							}
						})}
					</section>
					<section id="consejo" className="consejo">
						<h3 className="mt-5 d-flex justify-content-center">Vocales</h3><br />
						<hr className="hr" />
						<Swiper autoplay={{
							delay: 3500,
							disableOnInteraction: false,
						}}
							modules={[Pagination, Navigation]}
							pagination={{
								clickable: true,
							}}
							navigation={true}
							spaceBetween={30}
							slidesPerView={1}
							breakpoints={{
								320: {
									slidesPerView: 1,
									spaceBetween: 15,
								},
								640: {
									slidesPerView: 2,
									spaceBetween: 20,
								},
								768: {
									slidesPerView: 4,
									spaceBetween: 40,
								},
								1800: {
									slidesPerView: 4,
									spaceBetween: 50,
								},
							}}>


							{data.map((e) => {
								if ((e.Status) == 1) {
									return (
										<SwiperSlide>
											<div className='text-center justify-content-center'>
												<div className="card" >
													<img className={styles.act} src="/assets/img/index/1.webp" />
													<div className={styles.white}>
														<h5 className={styles.underlinedos}><span> {(e.Nombre)}</span></h5><br />
														<h6 className={styles.underlinedos}><span> {(e.Puesto)}</span></h6>
													</div>
												</div>
											</div>
										</SwiperSlide>
									)
								}
							})}
							<br />
						</Swiper>
					</section>
				</main>
			</Layout >
		</>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
