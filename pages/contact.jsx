import Layout from "@/components/layout";
import { Inter } from "next/font/google";
import styles from "@/styles/contact.module.css";
import Link from "next/link";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });


// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import 'swiper/css/navigation';

// import required modules
import { EffectCoverflow, Pagination, Navigation } from 'swiper/modules';

export default function Contact() {
	const t = useTranslations("Contact");
	const cel = [
		"6141234567"
	]
	return (
		<>
			<Layout>
				<section id="sobre" className="mb-5" >
					<div className="container mt-5">

						<div className={styles.titulo}>
							<h1 className="mb-3 d-flex justify-content-center text-uppercase fst-italic fw-bold"><span>{t(`contacto`)}</span></h1>
						</div>
						<div className={styles.half1}>
							<div className="row " >
								<div className="col-md-6">
									<div className="card-transparent ">
										<div className="card-body ">
										{/* 	<h3 className="mb-3 d-flex justify-content-center m-2">
												{t(`contacto`)}
											</h3> */}
											<h5 className="subtitle m-5">
												{t(`desc`)}
												<ul className="list-group list-group-flush mt-3">
													<li className="list-group-item">
														<a href="mailto:britaniachihuahua@gmail.com"><i className="bi bi-envelope-at flex-shrink-0"> {t('email')}</i></a>
													</li>
													<li className="list-group-item">
														{cel.map((tel, index) => (
															<Link target="_blank" key={index} href={`tel:+${tel}`}
																className="nav-item  link-underline-info mb-2"><i className="bi bi-telephone flex-shrink-0"> {t('num3')}</i></Link>
														))}
													</li>
													<li className="list-group-item">
														{cel.map((tel, index) => (
															<Link target="_blank" key={index} href={`https://api.whatsapp.com/send?phone=${tel}&text=Buenos%20dias`}
																className="nav-item  link-underline-info mb-2"><i className="bi bi-whatsapp"> {t('num1')}</i></Link>
														))}
													</li>
													<li className="list-group-item">
														{cel.map((tel, index) => (
															<Link target="_blank" key={index} href={`https://t.me/+521${tel}`}
																className="nav-item  link-underline-info mb-2"><i className="bi bi-telegram"> {t('num2')}</i></Link>
														))}
													</li>
												</ul>
											</h5>
										</div>
									</div>
								</div>
								<div className="col">
									<iframe className={styles.iframe} src={`https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d56033.69761278814!2d-106.112154!3d28.626582!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86ea435fb394a737%3A0x495d8a25661ca25d!2sClub%20Britania!5e0!3m2!1ses-419!2sus!4v1715108611468!5m2!1ses-419!2sus`}
										height="100%" allowFullScreen loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
								</div>
							</div>
						</div>
					</div>
				</section>
			</Layout>
		</>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
