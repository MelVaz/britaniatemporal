import Layout from "@/components/layout";
import { Inter } from "next/font/google";
import styles from "@/styles/newsplus.module.css";
import Link from "next/link";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import 'swiper/css/navigation';

// import required modules
import { EffectCoverflow, Pagination, Navigation } from 'swiper/modules';


/* db */
import * as React from 'react';
import { loadNew } from '@/pages/api/actions'

export default function Newsplus({ data }) {
	const t = useTranslations("Newsplus");
	return (
		<>
			<Layout>
				<section id="sobre" className=" mt-5">

					<div className={styles.titulo}>
						<h1 className="m-3 d-flex justify-content-center text-uppercase fst-italic fw-bold"><span>{t(`news`)}</span></h1>
					</div>
				</section>
				<section id="noticia1" className="m-5">
					<div className="container">
						<div className={styles.noticia}>
							<div className="row mt-5" >
								<h2 className="m-3  text-center">
									{data.title}
								</h2>
								<div className="col">
									<img src="/assets/img/index/1.webp" className="img-fluid mx-auto d-block" />
									<span className="d-flex justify-content-center mb-3 fw-bold fst-italic">{data.fecha}</span>
								</div>
							</div>
							<div className={styles.newspaper}>
								{data.description.split('.').map(d => <p className="subtitle mt-2 text-justify">{d}.</p>)}
							</div>
						</div>
					</div>
				</section>
				<section id="profesores" >
					<div className="container mb-5">
						<h3 className="mt-5 d-flex justify-content-center">Galeria</h3><br />
						<hr className="hr" />
						<Swiper
							effect={'coverflow'}
							grabCursor={true}
							centeredSlides={true}
							slidesPerView={3}
							initialSlide={1}
							coverflowEffect={{
								rotate: 50,
								stretch: 0,
								depth: 100,
								modifier: 1,
								slideShadows: true,
							}}
							pagination={true}
							modules={[EffectCoverflow, Pagination]}
							className={styles.swiper2}
						>
							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>

							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>
							<SwiperSlide className={styles.swiperslide2}>
								<img src="/assets/img/index/1.webp" />
							</SwiperSlide>
							<br /><br />
						</Swiper>
					</div>
				</section>
			</Layout>
		</>
	);
}

export async function getServerSideProps(props) {
	const id = await loadNew(props.params.id);
	/* console.log(id); */
	return { props: { data: id, messages: (await import(`../../messages/${props.locale}.json`)).default } };
}