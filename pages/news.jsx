import Layout from "@/components/layout";
import { Inter } from "next/font/google";
import styles from "@/styles/news.module.css";
import Link from "next/link";

// Import Translations
import { useTranslations } from 'next-intl';

const inter = Inter({ subsets: ["latin"] });

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import 'swiper/css/navigation';

// import required modules
import { EffectCoverflow, Pagination, Navigation } from 'swiper/modules';

/* db */
import * as React from 'react';
import { loadNews } from '@/pages/api/actions'
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useState } from "react";

export default function News() {
	const t = useTranslations("News");
	const [data, setData] = React.useState([]);
	React.useEffect(
		() => {
			loadNews().then((p) => setData(p));
		}, []
	)
	return (
		<>
			<Layout>
				<section id="sobre" className=" mt-5">
					<div className={styles.titulo}>
						<h1 className="mb-3 d-flex justify-content-center text-uppercase fst-italic fw-bold"><span>{t(`news`)}</span></h1>
					</div>
				</section>
				<section id="noticia" className=" mt-3">
					<div className="container">
						{data.map((e) => {
							if ((e.Status) == 1) {
								return (
									<div className={styles.noticia}>
										<div className="row mt-5" >
											<div className="col-md-6">
												<div className="card-transparent ">
													<div className="card-body ">
														<h3 className="m-3 text-center">
															{(e.Titulo)}
														</h3>
														<h5 className="subtitle m-3 text-justify">
															{(e.Descripcion.substring(0, 250))}...
														</h5>
														<span className=" m-3 text-justify fw-bolder fst-italic">{(e.Fecha)}</span> <br />
													</div>
													<div className="card-footer">
														<Link href={"/newsplus/" + e.id}><button type="button" className="btn btn-outline-primary m-4">{t('vermas')}</button></Link>
													</div>
												</div>
											</div>
											<div className="col">
												<img src="/assets/img/index/1.webp" className="img-fluid mx-auto d-block" />
											</div>
										</div>
									</div>
								)
							}
						})}
					</div>
				</section>
			</Layout>
		</>
	);
}
export async function getStaticProps(context) {
	return {
		props: {
			messages: (await import(`../messages/${context.locale}.json`)).default
		}
	};
}
