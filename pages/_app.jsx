import "@/styles/globals.css";
import { useRouter } from 'next/router';

import { Inter } from 'next/font/google'
const inter = Inter({ subsets: ['latin'] })

import { NextIntlClientProvider } from 'next-intl';
import { notFound } from 'next/navigation';

export function generateStaticParams() {
	return [{ locale: 'es' }, { locale: 'en' }];
}

export default function App({ Component, pageProps }) {
	const router = useRouter();

	return (
		<NextIntlClientProvider
			locale={router.locale}
			timeZone="Europe/Vienna"
			messages={pageProps.messages}
		>
			<Component {...pageProps} />
		</NextIntlClientProvider>
	);
}

